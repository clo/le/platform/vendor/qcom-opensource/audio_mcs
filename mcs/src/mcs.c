/*
 * Copyright (c) 2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Changes from Qualcomm Innovation Center, Inc. are provided under the following license:
 *
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#define LOG_TAG "MCS"

#include <tinyalsa/asoundlib.h>
#include <sound/asound.h>
#include "mcs.h"
#include "mcs_api.h"
#include <unistd.h>
#include <cutils/properties.h>
#include <errno.h>
#include <agm/agm_api.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <audio_route/audio_route.h>
#include <expat.h>
#include <agm/agm_list.h>

#define NO_OF_BUFS 4
#define REC_BUF_SIZE 1920
#define BUFFER_COUNT 32
#define SNDCARD 100
#define MAX_SND_CARD 10
#define MAX_RETRY_CNT 20
#define SND_CARD_VIRTUAL 100
#define MIXER_FILE_DELIMITER "_"
#define MIXER_FILE_EXT ".xml"
#define MIXER_PATH_MAX_LENGTH 100
#define HW_INFO_ARRAY_MAX_SIZE 32
#define VENDOR_CONFIG_PATH_MAX_LENGTH 128
#define XML_PATH_EXTN_MAX_SIZE 80

#define ADEV_INFO_XMLFILE_BASE_STRING_NAME "mcs_defs"
#define XML_FILE_DELIMITER "_"
#define XML_FILE_EXT ".xml"
#define XML_PATH_MAX_LENGTH 100
#define AUDIO_INTERFACE_RX "CODEC_DMA-LPAIF_WSA-RX-0"
#define AUDIO_INTERFACE_TX "CODEC_DMA-LPAIF_RXTX-TX-3"
#define PARAM_ID_MFC_OUTPUT_MEDIA_FORMAT 0x08001024

#if defined (LINUX_ENABLED)
    #define MIXER_XML_BASE_STRING_NAME "mixer_paths_wsa"
#elif defined (FEATURE_IPQ_OPENWRT)
    #define MIXER_XML_BASE_STRING_NAME "mixer_paths"
#else
    #define MIXER_XML_BASE_STRING_NAME "mixer_paths"
#endif

#define ID_RIFF 0x46464952
#define ID_WAVE 0x45564157
#define ID_FMT  0x20746d66
#define ID_DATA 0x61746164

#define FORMAT_PCM 1

struct mcs_play_ctxt {
    unsigned int cur_state;
    mcs_play_t cur_param;
    ar_fhandle file_handle;
    ar_osal_thread_t tid;
    ar_osal_thread_attr_t tattr;
    void *stream_handle;
    size_t buffer_size;
    unsigned int device;
    char intf_name[100];
    char snd_device_name[100];
    struct pcm *pcm;
};

struct mcs_rec_ctxt {
    unsigned int cur_state;
    unsigned int dkv;
    mcs_record_t cur_param;
    ar_fhandle file_handle;
    ar_osal_thread_t tid;
    ar_osal_thread_attr_t tattr;
    void *stream_handle;
    size_t buffer_size;
    unsigned int device;
    char intf_name[100];
    char snd_device_name[100];
    struct pcm *pcm;
};

struct acdb_mcs {
    struct mcs_play_ctxt* pb_ctxt;
    struct mcs_rec_ctxt* rec_ctxt;
    ar_osal_mutex_t lock;
    struct mixer *mixer;
    struct audio_route* audio_route;
    struct listnode devs_list;
};

struct device_config {
    char name[80];
    unsigned int sample_rate;
    unsigned int channels;
    unsigned int bit_width;
};

struct wav_header {
    uint32_t riff_id;
    uint32_t riff_sz;
    uint32_t riff_fmt;
    uint32_t fmt_id;
    uint32_t fmt_sz;
    uint16_t audio_format;
    uint16_t num_channels;
    uint32_t sample_rate;
    uint32_t byte_rate;
    uint16_t block_align;
    uint16_t bits_per_sample;
    uint32_t data_id;
    uint32_t data_sz;
};

/* Payload of the PARAM_ID_MFC_OUTPUT_MEDIA_FORMAT parameter in the
 Media Format Converter Module. Following this will be the variable payload for channel_map. */
struct param_id_mfc_output_media_fmt_t
{
   int32_t sampling_rate;
   int16_t bit_width;
   int16_t num_channels;
   uint16_t channel_type[0];
}__attribute__((packed));

struct apm_module_param_data_t
{
   uint32_t module_instance_id;
   uint32_t param_id;
   uint32_t param_size;
   uint32_t error_code;
};


struct gsl_module_id_info_entry {
   uint32_t module_id; /**< module id */
   uint32_t module_iid; /**< globally unique module instance id */
};
/**
 * Structure mapping the tag_id to module info (mid and miid)
 */
struct gsl_tag_module_info_entry {
   uint32_t tag_id; /**< tag id of the module */
   uint32_t num_modules; /**< number of modules matching the tag_id */
   struct gsl_module_id_info_entry module_entry[0]; /**< module list */
};

struct gsl_tag_module_info {
   uint32_t num_tags; /**< number of tags */
   struct gsl_tag_module_info_entry tag_module_entry[0];
      /**< variable payload of type struct gsl_tag_module_info_entry */
};

#define PADDING_8BYTE_ALIGN(x)  ((((x) + 7) & 7) ^ 7)

static struct acdb_mcs *mcs_info;

struct audio_mixer* audio_mixer = NULL;
char adev_info_xml_file[XML_PATH_MAX_LENGTH] = {0};

enum pcm_channel_map
{
   PCM_CHANNEL_L = 1,
   PCM_CHANNEL_R = 2,
   PCM_CHANNEL_C = 3,
   PCM_CHANNEL_LS = 4,
   PCM_CHANNEL_RS = 5,
   PCM_CHANNEL_LFE = 6,
   PCM_CHANNEL_CS = 7,
   PCM_CHANNEL_CB = PCM_CHANNEL_CS,
   PCM_CHANNEL_LB = 8,
   PCM_CHANNEL_RB = 9,
};

enum {
    MCS_STATE_IDLE,
    MCS_STATE_RUNNING,
    MCS_STATE_STOPPING
};

enum dir{
    PLAYBACK,
    CAPTURE,
    LOOPBACK,
};

enum stream_type {
    STREAM_PCM,
    STREAM_COMPRESS,
};

struct platform_xml_data{
    char data_buf[1024];
    size_t offs;
};

struct mcs_dev_def {
   int32_t key_value;
   char snd_device_name[24];
   char back_end_name[32];
   struct listnode list_node;
};

static int parse_and_update_audio_defs(char* adev_info_xml_file);
static void start_tag(void *userdata, const XML_Char *tag_name,
    const XML_Char **attr);
static void end_tag(void *userdata, const XML_Char *tag_name);
static void handle_data(void *userdata, const char *s, int len);
static void process_device_data(struct platform_xml_data *data,const XML_Char **attr);
static int32_t convert_char_to_hex(char* num);

static int set_device_media_config(struct mixer *mixer, unsigned int channels,
                unsigned int rate, unsigned int bits, char *intf_name);
static int set_metadata(struct mixer *mixer, int device, uint32_t val,
                enum stream_type stype, char *intf_name,
                mcs_play_t *mcs_param);

static unsigned int  bits_to_alsa_format(unsigned int bits)
{
    switch (bits) {
    case 32:
        return SNDRV_PCM_FORMAT_S32_LE;
    case 8:
        return SNDRV_PCM_FORMAT_S8;
    case 24:
        return SNDRV_PCM_FORMAT_S24_3LE;
    default:
    case 16:
        return SNDRV_PCM_FORMAT_S16_LE;
    };
}

static void getFileNameExtn(const char* in_snd_card_name, const char* file_name_extn,
                            char* file_name_extn_wo_variant)
{
    /* Sound card name follows below mentioned convention:
       <target name>-<form factor>-<variant>-snd-card.
    */
    char *snd_card_name = NULL;
    char *tmp = NULL;
    char *card_sub_str = NULL;

    snd_card_name = strdup(in_snd_card_name);

    if (snd_card_name == NULL) {
        AR_LOG_ERR(LOG_TAG,"snd_card_name passed is NULL");
        goto err;
    }

   card_sub_str = strtok_r(snd_card_name, "-", &tmp);
    if (card_sub_str == NULL) {
        AR_LOG_ERR(LOG_TAG,"called on invalid snd card name");
        goto err;
    }
    strlcat(file_name_extn,card_sub_str, XML_PATH_EXTN_MAX_SIZE);

    while ((card_sub_str = strtok_r(NULL, "-", &tmp))) {
           if (strncmp(card_sub_str, "snd", strlen("snd"))) {
               strlcpy(file_name_extn_wo_variant, file_name_extn, XML_PATH_EXTN_MAX_SIZE);
               strlcat(file_name_extn, MIXER_FILE_DELIMITER, XML_PATH_EXTN_MAX_SIZE);
               strlcat(file_name_extn, card_sub_str, XML_PATH_EXTN_MAX_SIZE);
           }
           else
               break;
    }

    AR_LOG_INFO(LOG_TAG,"file path extension(%s)", file_name_extn);
    AR_LOG_INFO(LOG_TAG,"file path extension without variant(%s)",
                           file_name_extn_wo_variant);
err:
    if (snd_card_name)
        free(snd_card_name);

}

static int set_agm_stream_metadata_type(int device, char *val,
                                       enum stream_type stype)
{
    char *stream = "PCM";
    char *control = "control";
    char *mixer_str;
    struct mixer_ctl *ctl;
    int ctl_len = 0,ret = 0;

    ctl_len = strlen(stream) + 4 + strlen(control) + 1;
    mixer_str = calloc(1, ctl_len);
    if (!mixer_str) {
        AR_LOG_ERR(LOG_TAG, "%s: memory allocation failed.", __func__);
        return -ENOMEM;
    }
    snprintf(mixer_str, ctl_len, "%s%d %s", stream, device, control);

    AR_LOG_VERBOSE(LOG_TAG, "%s - mixer -%s-\n", __func__, mixer_str);
    ctl = mixer_get_ctl_by_name(mcs_info->mixer, mixer_str);
    if (!ctl) {
        AR_LOG_ERR(LOG_TAG, "Invalid mixer control: %s\n", mixer_str);
        free(mixer_str);
        return -ENOENT;
    }

    ret = mixer_ctl_set_enum_by_string(ctl, val);
    free(mixer_str);
    return ret;
}

static unsigned int mcs_get_device_id(uint32_t device_id)
{
    uint32_t playback_device_id = 110, record_device_id = 111;
    if (device_id == DEVICERX)
         return playback_device_id;
    else if (device_id == DEVICETX)
         return record_device_id;
    return -1;

}

static int mcs_get_snd_device_name(uint32_t key_value, char *intf_name, int channels, uint8_t ch_mask)
{
    if(!list_empty(&mcs_info->devs_list)) {
        struct listnode *pv_pair_node;
        struct mcs_dev_def *pv_pair;
        list_for_each(pv_pair_node, &mcs_info->devs_list) {
            pv_pair = node_to_item(pv_pair_node, struct mcs_dev_def, list_node);
            if(pv_pair->key_value == key_value) {
                strlcpy(intf_name, pv_pair->snd_device_name, strlen(pv_pair->snd_device_name)+1);
                return 0;
            }
        }
        return -1;
    }

    if (key_value == SPEAKER) {
        strlcpy(intf_name, "speaker", 8);
        return 0;
    } else if (key_value == HANDSETMIC) {
        if (channels == 1)
            strlcpy(intf_name, "handset-mic", 12);
        else if (channels == 2)
            strlcpy(intf_name, "handset-dmic-endfire", 21);
        else if (channels == 3)
            strlcpy(intf_name, "three-mic", 12);
        else if (channels == 4)
            strlcpy(intf_name, "quad-mic", 9);
        return 0;
    } else if (key_value == HEADPHONES ) {
        strlcpy(intf_name, "headphones", 11);
        return 0;
    } else if (key_value == SPEAKER_MIC ) {
        strlcpy(intf_name, "speaker-mic", 12);
        return 0;
    } else if (key_value == VI_TX ) {
        if (channels == 1) {
            if (ch_mask == 1) {
                strlcpy(intf_name, "vi-feedback-mono-1", 19);
            } else if (ch_mask == 2) {
                strlcpy(intf_name, "vi-feedback-mono-2", 19);
            } else {
                AR_LOG_ERR(LOG_TAG,"Incorrect channel mask : %d for channel 1", ch_mask);
                return -1;
            }
        } else if (channels == 2) {
            strlcpy(intf_name, "vi-feedback", 12);
        } else {
            AR_LOG_ERR(LOG_TAG,"Incorrect channel number : %d", channels);
            return -1;
        }
        return 0;
    } else if (key_value == HEADPHONE_MIC ) {
        strlcpy(intf_name, "headset-mic", 12);
        return 0;
    }
    return -1;
}

static int mcs_get_intf_name(uint32_t key_value, char *intf_name)
{
    if(!list_empty(&mcs_info->devs_list)) {
        struct listnode *pv_pair_node;
        struct mcs_dev_def *pv_pair;
        list_for_each(pv_pair_node, &mcs_info->devs_list) {
            pv_pair = node_to_item(pv_pair_node, struct mcs_dev_def, list_node);
            if(pv_pair->key_value == key_value) {
               strlcpy(intf_name, pv_pair->back_end_name, strlen(pv_pair->back_end_name)+1);
               return 0;
            }
        }
        return -1;
    }

    if (key_value == SPEAKER) {
        strlcpy(intf_name, AUDIO_INTERFACE_RX, strlen(AUDIO_INTERFACE_RX) + 1);
        return 0;
    } else if (key_value == HANDSETMIC) {
        strlcpy(intf_name, AUDIO_INTERFACE_TX, strlen(AUDIO_INTERFACE_TX) + 1);
        return 0;
    } else if (key_value == VI_TX) {
        strlcpy(intf_name, "CODEC_DMA-LPAIF_WSA-TX-0",
                        strlen("CODEC_DMA-LPAIF_WSA-TX-0") + 1);
        return 0;
    } else if (key_value == HEADPHONES) {
        strlcpy(intf_name, "CODEC_DMA-LPAIF_RXTX-RX-0",
                        strlen("CODEC_DMA-LPAIF_RXTX-RX-0") + 1);
        return 0;
    } else if (key_value == SPEAKER_MIC) {
        strlcpy(intf_name, AUDIO_INTERFACE_TX, strlen(AUDIO_INTERFACE_TX) + 1);
        return 0;
    } else if (key_value == HEADPHONE_MIC) {
        strlcpy(intf_name, AUDIO_INTERFACE_TX, strlen(AUDIO_INTERFACE_TX) + 1);
        return 0;
    }
    AR_LOG_ERR(LOG_TAG,"Invalid Key_value:%d\n", key_value);
    return -1;
}

static int32_t mcs_record_prepare(struct mcs_rec_ctxt* ctxt)
{
    AR_LOG_VERBOSE(LOG_TAG,"enter: ctxt - %pK", ctxt);
    int ret = 0;
    size_t size;
    struct pcm_config config;
    unsigned int period_size = 1024;
    unsigned int period_count = 4;
    int bits = 0;
    int card = SND_CARD_VIRTUAL;

    memset(&config, 0, sizeof(config));
    if (ctxt == NULL) {
        AR_LOG_ERR(LOG_TAG, " null pointer");
        ret = AR_EBADPARAM;
        return ret;
    }
    if (ctxt->cur_param.stream_properties.sample_rate != 0)
        config.rate = ctxt->cur_param.stream_properties.sample_rate;
    else
        config.rate = 48000;
    if (ctxt->cur_param.stream_properties.bit_width != 0)
        bits = ctxt->cur_param.stream_properties.bit_width;
    else
        bits = 16;
    if (ctxt->cur_param.stream_properties.num_channels != 0)
        config.channels = ctxt->cur_param.stream_properties.num_channels;
    else
        config.channels = 1;
    config.period_size = period_size;
    config.period_count = period_count;

    if (bits == 32)
        config.format = PCM_FORMAT_S32_LE;
    else if (bits == 24)
        config.format = PCM_FORMAT_S24_3LE;
    else if (bits == 16)
        config.format = PCM_FORMAT_S16_LE;

    config.start_threshold = 0;
    config.stop_threshold = INT_MAX;
    config.silence_threshold = 0;

    if (ctxt->cur_param.write_to_file == 1) {
        ret = ar_fopen(&ctxt->file_handle, ctxt->cur_param.filename,
              AR_FOPEN_WRITE_ONLY);
        AR_LOG_VERBOSE(LOG_TAG,"ar_fread file handle %pK file name %s",
                         ctxt->file_handle, ctxt->cur_param.filename);
        if (ret != 0) {
            AR_LOG_ERR(LOG_TAG,"file open error");
            return ret;
        }
    }

    ctxt->pcm = pcm_open(card, ctxt->device, PCM_IN, &config);
    if (!ctxt->pcm || !pcm_is_ready(ctxt->pcm)) {
       AR_LOG_ERR(LOG_TAG, "Unable to open PCM device (%s)\n",
                pcm_get_error(ctxt->pcm));
        ret = -EINVAL;
        goto exit;
    }

    size = pcm_frames_to_bytes(ctxt->pcm, pcm_get_buffer_size(ctxt->pcm));
    ctxt->buffer_size = size;

    AR_LOG_VERBOSE(LOG_TAG, "Capturing sample: %u ch, %u hz, %u bit\n",
                      config.channels, config.rate,
                      pcm_format_to_bits(config.format));

    audio_route_apply_and_update_path(mcs_info->audio_route,
                                          ctxt->snd_device_name);
    ret = pcm_start(ctxt->pcm);
    if (ret < 0) {
        AR_LOG_ERR(LOG_TAG, "start error\n");
        goto err_pcm_close;
    }

    AR_LOG_VERBOSE(LOG_TAG, "exit");
    return ret;

err_pcm_close:
    pcm_close(ctxt->pcm);
exit:
    if (ctxt->file_handle != NULL){
        AR_LOG_INFO(LOG_TAG,"file closed");
        ar_fclose(ctxt->file_handle);
    }
    return ret;
}

static int32_t mcs_play_prepare(struct mcs_play_ctxt* ctxt)
{
    AR_LOG_VERBOSE(LOG_TAG,"enter: ctxt - %pK", ctxt);
    int ret = 0;
    size_t size;
    size_t in_buf_size = 0;
    struct pcm_config config;
    unsigned int period_size = 1024;
    unsigned int period_count = 4;
    unsigned int bits = 0;
    unsigned int card = SND_CARD_VIRTUAL;

    memset(&config, 0, sizeof(config));
    if (ctxt == NULL) {
        AR_LOG_ERR(LOG_TAG, " null pointer");
        ret = AR_EBADPARAM;
        return ret;
    }
    config.channels = ctxt->cur_param.stream_properties.num_channels;
    config.rate = ctxt->cur_param.stream_properties.sample_rate;
    bits = ctxt->cur_param.stream_properties.bit_width;
    config.period_size = period_size;
    config.period_count = period_count;

    if (bits == 32)
        config.format = PCM_FORMAT_S32_LE;
    else if (bits == 24)
        config.format = PCM_FORMAT_S24_3LE;
    else if (bits == 16)
        config.format = PCM_FORMAT_S16_LE;
    config.start_threshold = 0;
    config.stop_threshold = INT_MAX;
    config.silence_threshold = 0;


    size = ctxt->cur_param.stream_properties.num_channels *
           ctxt->cur_param.stream_properties.bit_width * BUFFER_COUNT;

    ret = ar_fopen(&ctxt->file_handle, ctxt->cur_param.filename,
          AR_FOPEN_READ_ONLY);
    AR_LOG_ERR(LOG_TAG, "%s:%d ar_fread file handle %pK",
                 __func__, __LINE__, ctxt->file_handle);
    if (ret != 0) {
        AR_LOG_ERR(LOG_TAG, "file open error");
        return ret;
    }

    ctxt->pcm = pcm_open(card, ctxt->device, PCM_OUT, &config);
    if (!ctxt->pcm || !pcm_is_ready(ctxt->pcm)) {
       AR_LOG_ERR(LOG_TAG, "Unable to open PCM device %u (%s)\n",
                ctxt->device, pcm_get_error(ctxt->pcm));
        ret -EINVAL;
        goto exit;
    }

    size = pcm_frames_to_bytes(ctxt->pcm, pcm_get_buffer_size(ctxt->pcm));
    ctxt->buffer_size = size;

    audio_route_apply_and_update_path(mcs_info->audio_route,
                                          ctxt->snd_device_name);
    ret = pcm_start(ctxt->pcm);
    if (ret < 0) {
        AR_LOG_ERR(LOG_TAG, "start error\n");
        goto err_pcm_close;
    }

    AR_LOG_VERBOSE(LOG_TAG, "exit");
    return ret;

err_pcm_close:
    pcm_close(ctxt->pcm);
exit:
    if (ctxt->file_handle != NULL)
        ar_fclose(ctxt->file_handle);
    return ret;
}

static int32_t mcs_play_close(struct mcs_play_ctxt* ctxt)
{
    AR_LOG_VERBOSE(LOG_TAG, "enter: ctxt - %pK", ctxt);
    int status = 0;
    if (ctxt == NULL) {
        AR_LOG_ERR(LOG_TAG, "null pointer");
        return AR_EBADPARAM;
    }

    status = pcm_stop(ctxt->pcm);
    if (status != 0) {
        AR_LOG_ERR(LOG_TAG, "stream stop failed");
    }

    status = pcm_close(ctxt->pcm);
    if (status != 0) {
        AR_LOG_ERR(LOG_TAG, "stream close failed");
    }
    if (ctxt->file_handle != NULL) {
        status = ar_fclose(ctxt->file_handle);
    }
    audio_route_reset_and_update_path(mcs_info->audio_route,
                                            ctxt->snd_device_name);
    AR_LOG_INFO(LOG_TAG, "exit");
    return status;
}

static void mcs_play_sample(void *txt)
{
    AR_LOG_VERBOSE(LOG_TAG, "enter: txt - %pK", txt);
    if (txt == NULL) {
        AR_LOG_ERR(LOG_TAG, "null pointer");
        return;
    }
    struct mcs_play_ctxt *ctxt = (struct mcs_play_ctxt *)txt;
    char * buffer = NULL;
    int status;
    size_t file_size = 0;
    size_t size, num_read, num_read1;
    int total_bytes_to_play = 0;
    int bytes_count = 0;
    int replay = 0;
    ar_fhandle file;
    if (ctxt->cur_param.playback_duration_sec > 0) {
        total_bytes_to_play = ctxt->cur_param.playback_duration_sec *
                              ctxt->cur_param.stream_properties.sample_rate *
                              ctxt->cur_param.stream_properties.num_channels *
                              ctxt->cur_param.stream_properties.bit_width / 8;
        replay = 1;
    }
    else
        return;
    size = ctxt->buffer_size;
    buffer = malloc(size);
    if (!buffer) {
        AR_LOG_ERR(LOG_TAG, "Unable to allocate %d bytes\n", size);
        goto fileclose;
    }
    file_size = ar_fsize(ctxt->file_handle);
    if (file_size == AR_EFAILED || file_size == AR_EBADPARAM) {
        AR_LOG_ERR(LOG_TAG, "file size error %d", file_size);
        if (replay == 1)
            goto err;
    }

    AR_LOG_INFO(LOG_TAG, "buffer address %pK", buffer);
    AR_LOG_VERBOSE(LOG_TAG, "playing sample\n");
    do {
        status = ar_fread(ctxt->file_handle, (void*)buffer, size, &num_read);
        AR_LOG_INFO(LOG_TAG, "ar_fread file handle %pK", ctxt->file_handle);
        if (status < 0) {
            AR_LOG_ERR(LOG_TAG, "ar read failed");
            break;
        }
        if (num_read > 0) {
            AR_LOG_INFO(LOG_TAG, "Before pcm_write\n");
            if (pcm_write(ctxt->pcm, buffer, num_read)) {
                AR_LOG_ERR(LOG_TAG, "Error playing sample\n");
                break;
            }

            bytes_count += num_read;
            AR_LOG_INFO(LOG_TAG, "%d bytes played", bytes_count);
            if (bytes_count >= total_bytes_to_play) {
                AR_LOG_INFO(LOG_TAG, "%d total bytes played", bytes_count);
                break;
            }
        } else if (num_read == 0) {
            if ((bytes_count % file_size) == 0) {
                status = pcm_stop(ctxt->pcm);
                AR_LOG_INFO(LOG_TAG, " pause playing sample");
                status = ar_fseek(ctxt->file_handle, 0, AR_FSEEK_BEGIN);
                AR_LOG_INFO(LOG_TAG, " fseek status = %d", status);
                num_read = size;
                status = pcm_start(ctxt->pcm);
                if (status < 0) {
                    AR_LOG_ERR(LOG_TAG, "start error\n");
                    goto err;
                }
                AR_LOG_INFO(LOG_TAG, " resume playing sample");
            }
        }
    } while ((ctxt->cur_state == MCS_STATE_RUNNING) && num_read > 0);
    AR_LOG_VERBOSE(LOG_TAG, "exit");
    free(buffer);
    return;
err:
    if (buffer)
        free(buffer);
fileclose:
    if (ctxt->file_handle)
        ar_fclose(ctxt->file_handle);
}

static int connect_audio_intf_to_stream(struct mixer *mixer, unsigned int device,
                    char *intf_name, enum stream_type stype, bool connect)
{
    char *stream = "PCM";
    char *control;
    char *mixer_str;
    struct mixer_ctl *ctl;
    int ctl_len = 0;
    int ret = 0;

    if (connect)
        control = "connect";
    else
        control = "disconnect";

    ctl_len = strlen(stream) + 4 + strlen(control) + 1;
    mixer_str = calloc(1, ctl_len);
    if (!mixer_str) {
        AR_LOG_ERR(LOG_TAG, "%s: memory allocation failed.", __func__);
        return -ENOMEM;
    }
    snprintf(mixer_str, ctl_len, "%s%d %s", stream, device, control);

    AR_LOG_VERBOSE(LOG_TAG, "%s - mixer -%s-\n", __func__, mixer_str);
    ctl = mixer_get_ctl_by_name(mixer, mixer_str);
    if (!ctl) {
        AR_LOG_ERR(LOG_TAG, "Invalid mixer control: %s\n", mixer_str);
        free(mixer_str);
        return -ENOENT;
    }

    ret = mixer_ctl_set_enum_by_string(ctl, intf_name);
    free(mixer_str);
    return ret;
}
int32_t process_playback_request(uint8_t cmd,
                                struct mcs_play_ctxt* ctxt,
                                mcs_play_t *param)
{
    AR_LOG_INFO(LOG_TAG, "enter cmd = %x", cmd);
    int ret = 0;
    int no_of_devices = 0;
    int i = 0;
    int j = 0;
    void *tid1 = NULL;
    void *tid2 = NULL;
    int no_of_kv_pairs = 0;
    unsigned int sample_rate = 44100;
    unsigned int channels = 1;
    unsigned int bit_width = 16;
    uint8_t ch_mask = 0;

    if (ctxt == NULL) {
        AR_LOG_ERR(LOG_TAG, "%s:%d null pointer", __func__, __LINE__);
        return AR_EBADPARAM;
    }

    switch(cmd) {
        case MCS_START:
            AR_LOG_INFO(LOG_TAG, "MCS Playback start\n");
            if (param == NULL || param->graph_key_vector.kvp == NULL ) {
                AR_LOG_ERR(LOG_TAG, "%s:%d null pointer", __func__, __LINE__);
                return AR_EBADPARAM;
            }
            if (ctxt->cur_state == MCS_STATE_IDLE) {
                memcpy(&ctxt->cur_param, param, sizeof(ctxt->cur_param));
                no_of_kv_pairs = param->graph_key_vector.num_kvps;
                for (i = 0; i < no_of_kv_pairs; i++) {
                    if (param->graph_key_vector.kvp[i].key == STREAMRX) {
                        sample_rate = param->device_properties.sample_rate;
                        bit_width = param->device_properties.bit_width;
                        channels = param->device_properties.num_channels;
                    }
                    else if (param->graph_key_vector.kvp[i].key == DEVICERX) {
                        no_of_devices++;
                        ctxt->device = mcs_get_device_id(param->graph_key_vector.kvp[i].key);
                        if (ctxt->device < 0) {
                            AR_LOG_ERR(LOG_TAG, "error in getting device_id");
                            return ctxt->device;
                        }
                        ret = mcs_get_intf_name(param->graph_key_vector.kvp[i].value,
                                                              ctxt->intf_name);
                        if (ret) {
                            AR_LOG_ERR(LOG_TAG, "error in getting audio_intf name");
                            return ret;
                        }
                        ret = mcs_get_snd_device_name(param->graph_key_vector.kvp[i].value,
                                                       ctxt->snd_device_name, channels, ch_mask);
                        if (ret) {
                            AR_LOG_ERR(LOG_TAG, "error in getting snd device name");
                            return ret;
                        }
                    }
                }
            }
            else {
                AR_LOG_ERR(LOG_TAG, " current state is not proper");
                ret = -EINVAL;
                return ret;
            }
            AR_LOG_INFO(LOG_TAG, "no of devices %d %pK", no_of_devices, ctxt);

           /* set device/audio_intf media config mixer control */
            if (set_device_media_config(mcs_info->mixer, channels, sample_rate,
                                                bit_width, ctxt->intf_name)) {
                AR_LOG_INFO(LOG_TAG, "Failed to set device media config\n");
                return AR_EFAILED;
            }
          /* set audio interface metadata mixer control */
            if (set_metadata(mcs_info->mixer, ctxt->device, PCM_LL_PLAYBACK,
                                                  STREAM_PCM, NULL, param)) {
                AR_LOG_INFO(LOG_TAG, "Failed to set pcm metadata\n");
                return AR_EFAILED;
            }

            if (connect_audio_intf_to_stream(mcs_info->mixer, ctxt->device,
                                        ctxt->intf_name, STREAM_PCM, true)) {
                AR_LOG_ERR(LOG_TAG, "Failed to connect pcm to audio interface\n");
                return AR_EFAILED;
            }

            ret = mcs_play_prepare(ctxt);
            if (ret == 0) {
                ctxt->cur_state = MCS_STATE_RUNNING;
                ret = ar_osal_thread_create(&mcs_info->pb_ctxt->tid,
                                  &mcs_info->pb_ctxt->tattr, mcs_play_sample,
                                                               (void *)ctxt);
                if (ret != 0) {
                    ctxt->cur_state = MCS_STATE_IDLE;
                    AR_LOG_ERR(LOG_TAG, "MCS Playback start thread failed\n");
                }
            }
            else {
                AR_LOG_ERR(LOG_TAG, "MCS Playback start failed\n");
            }
            break;
        case MCS_STOP:
            AR_LOG_INFO(LOG_TAG, "MCS Playback stop\n");
            if (ctxt->cur_state != MCS_STATE_IDLE) {
                AR_LOG_INFO(LOG_TAG, "%s:%d mcs_stop called", __func__, __LINE__);
                ctxt->cur_state = MCS_STATE_STOPPING;
                ret = ar_osal_thread_join_destroy(mcs_info->pb_ctxt->tid);
                ret = mcs_play_close(ctxt);
                ctxt->cur_state = MCS_STATE_IDLE;
                AR_LOG_INFO(LOG_TAG, "%s:%d Play stopped", __func__, __LINE__);
            }
            break;
        default:
            AR_LOG_ERR(LOG_TAG, "cmd not found  %x ", cmd);
            ret = AR_EFAILED;
            break;
    }
    AR_LOG_VERBOSE(LOG_TAG, "exit");
    return ret;
}

static int set_device_media_config(struct mixer *mixer, unsigned int channels,
                 unsigned int rate, unsigned int bits, char *intf_name)
{
    char *control = "rate ch fmt";
    char *mixer_str;
    struct mixer_ctl *ctl;
    long media_config[4];
    int ctl_len = 0;
    int ret = 0;

    ctl_len = strlen(intf_name)+1+strlen(control)+1;

    mixer_str = calloc(1, ctl_len);
    if (!mixer_str) {
        AR_LOG_ERR(LOG_TAG, "%s: memory allocation failed.", __func__);
        return -ENOMEM;
    }
    snprintf(mixer_str, ctl_len, "%s %s", intf_name, control);

    AR_LOG_VERBOSE(LOG_TAG, "%s - mixer -%s-\n", __func__, mixer_str);
    ctl = mixer_get_ctl_by_name(mixer, mixer_str);
    if (!ctl) {
        AR_LOG_ERR(LOG_TAG, "Invalid mixer control: %s\n", mixer_str);
        free(mixer_str);
        return -ENOENT;
    }

    media_config[0] = rate;
    media_config[1] = channels;
    media_config[2] = bits_to_alsa_format(bits);
    media_config[3] = AGM_DATA_FORMAT_FIXED_POINT;

    AR_LOG_VERBOSE(LOG_TAG, "%s - %d - %d - %d\n", __func__, media_config[0],
                                             media_config[1], media_config[2]);
    ret = mixer_ctl_set_array(ctl, &media_config,
                                sizeof(media_config)/sizeof(media_config[0]));
    free(mixer_str);
    return ret;
}


static int set_metadata(struct mixer *mixer, int device, uint32_t val,
                 enum stream_type stype, char *intf_name,
                 mcs_play_t *mcs_param)
{
    char *stream = "PCM";
    char *control = "metadata";
    char *mixer_str;
    struct mixer_ctl *ctl;
    uint8_t *metadata = NULL;
    struct agm_key_value *gkv = NULL, *ckv = NULL;
    struct prop_data *prop = NULL;
    uint32_t num_gkv = 1, num_ckv = 1, num_props = 0;
    uint32_t gkv_size, ckv_size, prop_size, index = 0;
    int ctl_len = 0, ret = 0, offset = 0;
    char *type = "ZERO";

    if (intf_name)
        type = intf_name;

    ret = set_agm_stream_metadata_type(device, type, stype);
    if (ret)
        return ret;

    num_gkv = mcs_param->graph_key_vector.num_kvps;
    gkv_size = num_gkv * sizeof(struct agm_key_value);
    ckv_size = num_ckv * sizeof(struct agm_key_value);
    prop_size = sizeof(struct prop_data) + (num_props * sizeof(uint32_t));

    metadata = calloc(1, sizeof(num_gkv) + sizeof(num_ckv) + gkv_size +
                                                 ckv_size + prop_size);
    if (!metadata) {
        AR_LOG_ERR(LOG_TAG, "%s: memory allocation failed.", __func__);
        ret = -ENOMEM;
        goto err_ret;
    }

    ckv = calloc(num_ckv, sizeof(struct agm_key_value));
    if (!ckv) {
        AR_LOG_ERR(LOG_TAG, "%s: memory allocation failed.", __func__);
        ret = -ENOMEM;
        goto free_metadata;
    }

    prop = calloc(1, prop_size);
    if (!prop) {
        AR_LOG_ERR(LOG_TAG, "%s: memory allocation failed.", __func__);
        ret = -ENOMEM;
        goto free_ckv;
    }

    index = 0;
    ckv[index].key = VOLUME;
    ckv[index].value = LEVEL_0;

    prop->prop_id = 0;  //Update prop_id here
    prop->num_values = num_props;

    memcpy(metadata, &num_gkv, sizeof(num_gkv));
    offset += sizeof(num_gkv);
    memcpy(metadata + offset, mcs_param->graph_key_vector.kvp, gkv_size);
    offset += gkv_size;
    memcpy(metadata + offset, &num_ckv, sizeof(num_ckv));

    offset += sizeof(num_ckv);
    memcpy(metadata + offset, ckv, ckv_size);
    offset += ckv_size;
    memcpy(metadata + offset, prop, prop_size);

    ctl_len = strlen(stream) + 4 + strlen(control) + 1;
    /* Here the value 4 represents the lenght of the device_id passed*/

    mixer_str = calloc(1, ctl_len);
    if (!mixer_str) {
        AR_LOG_ERR(LOG_TAG, "%s: memory allocation failed.", __func__);
        ret = -ENOMEM;
        goto free_prop;
    }
    snprintf(mixer_str, ctl_len, "%s%d %s", stream, device, control);

    AR_LOG_VERBOSE(LOG_TAG, "%s - mixer -%s-\n", __func__, mixer_str);
    ctl = mixer_get_ctl_by_name(mixer, mixer_str);
    if (!ctl) {
        AR_LOG_ERR(LOG_TAG, "Invalid mixer control: %s\n", mixer_str);
        ret = -ENOENT;
        goto free_mixer_str;
    }

    ret = mixer_ctl_set_array(ctl, metadata, sizeof(num_gkv) + sizeof(num_ckv) +
                                             gkv_size + ckv_size + prop_size);

free_mixer_str:
    free(mixer_str);
free_prop:
    free(prop);
free_ckv:
    free(ckv);
free_metadata:
    free(metadata);
err_ret:
    return ret;
}


static void mcs_record_sample(void *txt)
{
    AR_LOG_VERBOSE(LOG_TAG, "enter : txt %pK", txt);
    struct mcs_rec_ctxt* ctxt = (struct mcs_rec_ctxt*)txt;
    char * buffer;
    int status;
    size_t size, num_read;
    int total_bytes_to_record = 0;
    int bytes_count = 0;
    ssize_t bytes_read = 0;
    int replay = 0;
    struct wav_header header;

    header.riff_id = ID_RIFF;
    header.riff_sz = 0;
    header.riff_fmt = ID_WAVE;
    header.fmt_id = ID_FMT;
    header.fmt_sz = 16;
    header.audio_format = FORMAT_PCM;
    header.num_channels = ctxt->cur_param.stream_properties.num_channels;
    header.sample_rate = ctxt->cur_param.stream_properties.sample_rate;
    header.bits_per_sample = ctxt->cur_param.stream_properties.bit_width;
    header.byte_rate = (header.bits_per_sample / 8) * header.num_channels * header.sample_rate;
    header.block_align = header.num_channels * (header.bits_per_sample / 8);
    header.data_id = ID_DATA;

    ar_fseek(ctxt->file_handle, sizeof(struct wav_header), 0);

    if (ctxt->cur_param.record_duration_sec > 0) {
        total_bytes_to_record = ctxt->cur_param.record_duration_sec *
                    ctxt->cur_param.stream_properties.sample_rate *
                    ctxt->cur_param.stream_properties.num_channels *
                    ctxt->cur_param.stream_properties.bit_width / 8;
        replay = 1;
    }

    size = ctxt->buffer_size;
    buffer = malloc(size);
    if (!buffer) {
        AR_LOG_ERR(LOG_TAG, "Unable to allocate %d bytes\n",size);
        goto fileclose;
    }

    while (ctxt->cur_state == MCS_STATE_RUNNING) {
        bytes_read = pcm_read(ctxt->pcm, buffer, size);
        if (ctxt->cur_param.write_to_file == 1) {
            AR_LOG_INFO(LOG_TAG, "bytes recorded =%d ", bytes_read);
            status = ar_fwrite(ctxt->file_handle, buffer, size, &num_read);
            AR_LOG_INFO(LOG_TAG, "ar_fread file handle %pK", ctxt->file_handle);
            bytes_count += num_read;
            if ((ctxt->cur_param.record_duration_sec > 0) &&
                (bytes_count >= total_bytes_to_record)) {
                AR_LOG_INFO(LOG_TAG, "total bytes recorded =%d ", bytes_count);
                break;
            }
        }
    }

    header.data_sz = pcm_bytes_to_frames(ctxt->pcm, bytes_read) * header.block_align;
    header.riff_sz = header.data_sz + sizeof(header) - 8;
    ar_fseek(ctxt->file_handle, 0, 0);
    ar_fwrite(ctxt->file_handle, &header, sizeof(struct wav_header), &num_read);

    AR_LOG_VERBOSE(LOG_TAG, "exit");
    if (buffer)
        free(buffer);
    return;
fileclose:
    if (ctxt->file_handle)
        ar_fclose(ctxt->file_handle);

}

static int32_t mcs_record_close(struct mcs_rec_ctxt* ctxt)
{
    AR_LOG_VERBOSE(LOG_TAG, "enter - ctxt %pK", ctxt);
    int status = 0;
    if (ctxt == NULL) {
        AR_LOG_ERR(LOG_TAG, " null pointer");
        return AR_EBADPARAM;
    }
    status = pcm_stop(ctxt->pcm);
    if (status != 0) {
        AR_LOG_ERR(LOG_TAG, "stream stop failed");
    }
    status = pcm_close(ctxt->pcm);
    if (status != 0) {
        AR_LOG_ERR(LOG_TAG, "stream close failed");
    }
    if (ctxt->cur_param.write_to_file == 1) {
        if (ctxt->file_handle != NULL) {
            status = ar_fclose(ctxt->file_handle);
        }
    }

    audio_route_reset_and_update_path(mcs_info->audio_route,
                                            ctxt->snd_device_name);
    AR_LOG_INFO(LOG_TAG, "exit");
    return status;
}
int set_audio_intf_metadata(struct mixer *mixer, char *intf_name, unsigned int dkv,
                                enum dir usecase, int rate, int bitwidth, uint32_t val)
{
    char *control = "metadata";
    struct mixer_ctl *ctl;
    char *mixer_str;
    struct agm_key_value *gkv = NULL, *ckv = NULL;
    struct prop_data *prop = NULL;
    uint8_t *metadata = NULL;
    uint32_t num_gkv = 1, num_ckv = 2, num_props = 0;
    uint32_t gkv_size, ckv_size, prop_size, ckv_index = 0;
    int ctl_len = 0, offset = 0;
    int ret = 0;

    gkv_size = num_gkv * sizeof(struct agm_key_value);
    ckv_size = num_ckv * sizeof(struct agm_key_value);
    prop_size = sizeof(struct prop_data) + (num_props * sizeof(uint32_t));

    metadata = calloc(1, sizeof(num_gkv) + sizeof(num_ckv) + gkv_size + ckv_size + prop_size);
    if (!metadata)
        return -ENOMEM;

    gkv = calloc(num_gkv, sizeof(struct agm_key_value));
    ckv = calloc(num_ckv, sizeof(struct agm_key_value));
    prop = calloc(1, prop_size);
    if (!gkv || !ckv || !prop) {
        if (ckv)
            free(ckv);
        if (gkv)
            free(gkv);
        free(metadata);
        return -ENOMEM;
    }

    if (usecase == PLAYBACK) {
        gkv[0].key = DEVICERX;
        gkv[0].value = dkv ? dkv : SPEAKER;
    }
    else {
        gkv[0].key = DEVICETX;
        gkv[0].value = dkv ? dkv : HANDSETMIC;
    }
    ckv[ckv_index].key = SAMPLINGRATE;
    ckv[ckv_index].value = rate;

    ckv_index++;
    ckv[ckv_index].key = BITWIDTH;
    ckv[ckv_index].value = bitwidth;

    prop->prop_id = 0;  //Update prop_id here
    prop->num_values = num_props;

    memcpy(metadata, &num_gkv, sizeof(num_gkv));
    offset += sizeof(num_gkv);
    memcpy(metadata + offset, gkv, gkv_size);
    offset += gkv_size;
    memcpy(metadata + offset, &num_ckv, sizeof(num_ckv));
    offset += sizeof(num_ckv);
    memcpy(metadata + offset, ckv, ckv_size);
    offset += ckv_size;
    memcpy(metadata + offset, prop, prop_size);

    ctl_len = strlen(intf_name) + 1 + strlen(control) + 1;
    mixer_str = calloc(1, ctl_len);
    if (!mixer_str) {
        free(metadata);
        return -ENOMEM;
    }
    snprintf(mixer_str, ctl_len, "%s %s", intf_name, control);

    ctl = mixer_get_ctl_by_name(mixer, mixer_str);
    if (!ctl) {
        AR_LOG_ERR(LOG_TAG,"Invalid mixer control: %s\n", mixer_str);
        free(gkv);
        free(ckv);
        free(prop);
        free(metadata);
        free(mixer_str);
        return ENOENT;
    }

    ret = mixer_ctl_set_array(ctl, metadata, sizeof(num_gkv) + sizeof(num_ckv) + gkv_size + ckv_size + prop_size);

    free(gkv);
    free(ckv);
    free(prop);
    free(metadata);
    free(mixer_str);
    return ret;
}
int agm_mixer_get_miid(struct mixer *mixer, int device, char *intf_name,
                       enum stream_type stype, int tag_id, uint32_t *miid)
{
    char *stream = "PCM";
    char *control = "getTaggedInfo";
    char *mixer_str;
    struct mixer_ctl *ctl;
    int ctl_len = 0,ret = 0, i;
    void *payload;
    struct gsl_tag_module_info *tag_info;
    struct gsl_tag_module_info_entry *tag_entry;
    int offset = 0;

    ret = set_agm_stream_metadata_type(device, intf_name, stype);
    if (ret)
        return ret;

    if (stype == STREAM_COMPRESS)
        stream = "COMPRESS";

    ctl_len = strlen(stream) + 4 + strlen(control) + 1;
    mixer_str = calloc(1, ctl_len);
    if (!mixer_str)
        return -ENOMEM;

    snprintf(mixer_str, ctl_len, "%s%d %s", stream, device, control);

    ctl = mixer_get_ctl_by_name(mixer, mixer_str);
    if (!ctl) {
        AR_LOG_ERR(LOG_TAG, "Invalid mixer control: %s\n", mixer_str);
        free(mixer_str);
        return ENOENT;
    }

    payload = calloc(1024, sizeof(char));
    if (!payload) {
        free(mixer_str);
        return -ENOMEM;
    }

    ret = mixer_ctl_get_array(ctl, payload, 1024);
    if (ret < 0) {
        AR_LOG_ERR(LOG_TAG, "Failed to mixer_ctl_get_array\n");
        free(payload);
        free(mixer_str);
        return ret;
    }
    tag_info = payload;
    ret = -1;
    tag_entry = &tag_info->tag_module_entry[0];
    offset = 0;
    for (i = 0; i < tag_info->num_tags; i++) {
        tag_entry += offset/sizeof(struct gsl_tag_module_info_entry);

        offset = sizeof(struct gsl_tag_module_info_entry) + (tag_entry->num_modules * sizeof(struct gsl_module_id_info_entry));
        if (tag_entry->tag_id == tag_id) {
            struct gsl_module_id_info_entry *mod_info_entry;

            if (tag_entry->num_modules) {
                 mod_info_entry = &tag_entry->module_entry[0];
                 *miid = mod_info_entry->module_iid;
                 ret = 0;
                 break;
            }
        }
    }

    free(payload);
    free(mixer_str);
    return ret;
}

void populateChannelMap(uint16_t *pcmChannel, uint8_t numChannel)
{
    if (numChannel == 1) {
        pcmChannel[0] = PCM_CHANNEL_C;
    } else if (numChannel == 2) {
        pcmChannel[0] = PCM_CHANNEL_L;
        pcmChannel[1] = PCM_CHANNEL_R;
    } else if (numChannel == 3) {
        pcmChannel[0] = PCM_CHANNEL_L;
        pcmChannel[1] = PCM_CHANNEL_R;
        pcmChannel[2] = PCM_CHANNEL_C;
    } else if (numChannel == 4) {
        pcmChannel[0] = PCM_CHANNEL_L;
        pcmChannel[1] = PCM_CHANNEL_R;
        pcmChannel[2] = PCM_CHANNEL_LB;
        pcmChannel[3] = PCM_CHANNEL_RB;
    }  else if (numChannel == 5) {
        pcmChannel[0] = PCM_CHANNEL_L;
        pcmChannel[1] = PCM_CHANNEL_R;
        pcmChannel[2] = PCM_CHANNEL_C;
        pcmChannel[3] = PCM_CHANNEL_LB;
        pcmChannel[4] = PCM_CHANNEL_RB;
    }  else if (numChannel == 6) {
        pcmChannel[0] = PCM_CHANNEL_L;
        pcmChannel[1] = PCM_CHANNEL_R;
        pcmChannel[2] = PCM_CHANNEL_C;
        pcmChannel[3] = PCM_CHANNEL_LFE;
        pcmChannel[4] = PCM_CHANNEL_LB;
        pcmChannel[5] = PCM_CHANNEL_RB;
    }  else if (numChannel == 7) {
        pcmChannel[0] = PCM_CHANNEL_L;
        pcmChannel[1] = PCM_CHANNEL_R;
        pcmChannel[2] = PCM_CHANNEL_C;
        pcmChannel[3] = PCM_CHANNEL_LFE;
        pcmChannel[4] = PCM_CHANNEL_LB;
        pcmChannel[5] = PCM_CHANNEL_RB;
        pcmChannel[6] = PCM_CHANNEL_CS;
    }  else if (numChannel == 8) {
        pcmChannel[0] = PCM_CHANNEL_L;
        pcmChannel[1] = PCM_CHANNEL_R;
        pcmChannel[2] = PCM_CHANNEL_C;
        pcmChannel[3] = PCM_CHANNEL_LFE;
        pcmChannel[4] = PCM_CHANNEL_LB;
        pcmChannel[5] = PCM_CHANNEL_RB;
        pcmChannel[6] = PCM_CHANNEL_LS;
        pcmChannel[7] = PCM_CHANNEL_RS;
    }
}

int agm_mixer_set_param(struct mixer *mixer, int device,
                        enum stream_type stype, void *payload, int size)
{
    char *stream = "PCM";
    char *control = "setParam";
    char *mixer_str;
    struct mixer_ctl *ctl;
    int ctl_len = 0,ret = 0;

    ctl_len = strlen(stream) + 4 + strlen(control) + 1;
    mixer_str = calloc(1, ctl_len);
    if (!mixer_str) {
        free(payload);
        return -ENOMEM;
    }
    snprintf(mixer_str, ctl_len, "%s%d %s", stream, device, control);

    ctl = mixer_get_ctl_by_name(mixer, mixer_str);
    if (!ctl) {
        AR_LOG_ERR(LOG_TAG, "Invalid mixer control: %s\n", mixer_str);
        free(mixer_str);
        return ENOENT;
    }


    ret = mixer_ctl_set_array(ctl, payload, size);

    free(mixer_str);
    return ret;
}

int configure_mfc(struct mixer *mixer, int device, char *intf_name, int tag,
                  enum stream_type stype, unsigned int rate,
                  unsigned int channels, unsigned int bits)
{
    int ret = 0;
    uint32_t miid = 0;
    struct apm_module_param_data_t* header = NULL;
    struct param_id_mfc_output_media_fmt_t *mfcConf;
    uint16_t* pcmChannel = NULL;
    uint8_t* payloadInfo = NULL;
    size_t payloadSize = 0, padBytes = 0, size;

    ret = agm_mixer_get_miid(mixer, device, intf_name, stype, tag, &miid);
    if (ret) {
        AR_LOG_ERR(LOG_TAG, "%s Get MIID from tag data failed\n", __func__);
        return ret;
    }

    payloadSize = sizeof(struct apm_module_param_data_t) +
                  sizeof(struct param_id_mfc_output_media_fmt_t) +
                  sizeof(uint16_t)*channels;

    padBytes = PADDING_8BYTE_ALIGN(payloadSize);

    payloadInfo = (uint8_t*) calloc(1, payloadSize + padBytes);
    if (!payloadInfo) {
        return -ENOMEM;
    }

    header = (struct apm_module_param_data_t*)payloadInfo;
    mfcConf = (struct param_id_mfc_output_media_fmt_t*)(payloadInfo +
               sizeof(struct apm_module_param_data_t));
    pcmChannel = (uint16_t*)(payloadInfo + sizeof(struct apm_module_param_data_t) +
                                       sizeof(struct param_id_mfc_output_media_fmt_t));

    header->module_instance_id = miid;
    header->param_id = PARAM_ID_MFC_OUTPUT_MEDIA_FORMAT;
    header->error_code = 0x0;
    header->param_size = payloadSize - sizeof(struct apm_module_param_data_t);

    mfcConf->sampling_rate = rate;
    mfcConf->bit_width = bits;
    mfcConf->num_channels = channels;
    populateChannelMap(pcmChannel, channels);
    size = payloadSize + padBytes;

    return agm_mixer_set_param(mixer, device, stype, (void *)payloadInfo, (int)size);

}


int32_t process_record_request(uint8_t cmd,
                                struct mcs_rec_ctxt* ctxt,
                                mcs_record_t *param)
{
    AR_LOG_INFO(LOG_TAG, "enter - ctxt %pK", ctxt);
    AR_LOG_INFO(LOG_TAG, "enter cmd = %x", cmd);
    int ret = 0;
    int no_of_devices = 0;
    int i = 0;
    int j = 0;
    void *tid1 = NULL;
    void *tid2 = NULL;
    int no_of_kv_pairs = 0;
    uint8_t ch_mask = 0;
    struct device_config dev_config, stream_config;
    dev_config.sample_rate = 48000;
    dev_config.channels = 1;
    dev_config.bit_width = 16;
    stream_config.sample_rate = 48000;
    stream_config.channels = 1;
    stream_config.bit_width = 16;

    if (ctxt == NULL) {
        AR_LOG_ERR(LOG_TAG, "%s:%d null pointer", __func__, __LINE__);
        return AR_EBADPARAM;
    }

    switch(cmd) {
        case MCS_START:
            AR_LOG_INFO(LOG_TAG, "MCS Record Start");
            if (param == NULL || param->graph_key_vector.kvp == NULL) {
                AR_LOG_ERR(LOG_TAG, "%s:%d null pointer", __func__, __LINE__);
                return AR_EBADPARAM;
            }
            if (ctxt->cur_state == MCS_STATE_IDLE) {
                memcpy(&ctxt->cur_param, param, sizeof(ctxt->cur_param));
                no_of_kv_pairs = param->graph_key_vector.num_kvps;
                for (i = 0; i < no_of_kv_pairs; i++) {
                    if (param->graph_key_vector.kvp[i].key == STREAMTX) {
                       if (param->stream_properties.sample_rate != 0)
                           stream_config.sample_rate = param->stream_properties.sample_rate;
                       if (param->stream_properties.bit_width != 0)
                           stream_config.bit_width = param->stream_properties.bit_width;
                       if (param->stream_properties.num_channels != 0)
                           stream_config.channels = param->stream_properties.num_channels;
                    }
                    else if (param->graph_key_vector.kvp[i].key == DEVICETX) {
                        no_of_devices++;
                        ctxt->device = mcs_get_device_id(param->graph_key_vector.kvp[i].key);
                        if (ctxt->device < 0) {
                            AR_LOG_ERR(LOG_TAG, "error in getting device_id");
                            return ctxt->device;
                        }
                        ret = mcs_get_intf_name(param->graph_key_vector.kvp[i].value,
                                                              ctxt->intf_name);
                        ctxt->dkv = param->graph_key_vector.kvp[i].value;
                        if (ret) {
                            AR_LOG_ERR(LOG_TAG, "error in getting audio_intf name");
                            return ret;
                        }

                        if (param->device_properties.sample_rate != 0)
                            dev_config.sample_rate = param->device_properties.sample_rate;
                        if (param->device_properties.bit_width != 0)
                            dev_config.bit_width = param->device_properties.bit_width;
                        if (param->device_properties.num_channels != 0) {
                            dev_config.channels = param->device_properties.num_channels;
                            if (dev_config.channels == 1) {
                                ch_mask = param->device_properties.channel_mapping[0];
                            }
                        }

                        ret = mcs_get_snd_device_name(param->graph_key_vector.kvp[i].value,
                                                       ctxt->snd_device_name, dev_config.channels, ch_mask);
                        if (ret) {
                            AR_LOG_ERR(LOG_TAG, "error in getting snd device name");
                            return AR_EFAILED;
                        }
                    }
                }
            }
            else {
                AR_LOG_ERR(LOG_TAG, " current state is not proper");
                ret = -EINVAL;
                return ret;
            }
            if (set_device_media_config(mcs_info->mixer, dev_config.channels, dev_config.sample_rate,
                                               dev_config.bit_width, ctxt->intf_name)) {
                AR_LOG_INFO(LOG_TAG, "Failed to set device media config\n");
                return AR_EFAILED;
            }

          /* set audio interface metadata mixer control backend_conf.xml*/
            if (set_audio_intf_metadata(mcs_info->mixer, ctxt->intf_name, ctxt->dkv, CAPTURE,
                                               dev_config.sample_rate, dev_config.bit_width, PCM_RECORD)) {
                AR_LOG_INFO(LOG_TAG, "Failed to set device metadata\n");
                return AR_EFAILED;
            }

            if (set_metadata(mcs_info->mixer, ctxt->device, PCM_RECORD,
                                                 STREAM_PCM, NULL, param)) {
                AR_LOG_INFO(LOG_TAG, "Failed to set pcm metadata\n");
                return AR_EFAILED;
            }
            if (configure_mfc(mcs_info->mixer, ctxt->device, ctxt->intf_name, TAG_STREAM_MFC,
                                    STREAM_PCM, stream_config.sample_rate,
                                    stream_config.channels, stream_config.bit_width)) {
                AR_LOG_INFO(LOG_TAG, "Failed to configure stream mfc\n");
            }


            if (connect_audio_intf_to_stream(mcs_info->mixer, ctxt->device,
                                       ctxt->intf_name, STREAM_PCM, true)) {
                AR_LOG_ERR(LOG_TAG, "Failed to connect pcm to audio interface\n");
                return AR_EFAILED;
            }
            ret = mcs_record_prepare(ctxt);
            if (ret == 0) {
                ctxt->cur_state = MCS_STATE_RUNNING;
                ret = ar_osal_thread_create(&mcs_info->rec_ctxt->tid,
                                              &mcs_info->rec_ctxt->tattr,
                                              mcs_record_sample, (void *)ctxt);
                if (ret != 0) {
                    ctxt->cur_state = MCS_STATE_IDLE;
                    AR_LOG_ERR(LOG_TAG, "Unable to create thread for record");
                }
            }
            else {
                AR_LOG_ERR(LOG_TAG, "Record Start failed\n");
            }
            break;

        case MCS_STOP:
            AR_LOG_INFO(LOG_TAG, "MCS Record Stop");
            if (ctxt->cur_state != MCS_STATE_IDLE) {
                AR_LOG_INFO(LOG_TAG, "%s:%d", __func__, __LINE__);
                ctxt->cur_state = MCS_STATE_STOPPING;
                ret = mcs_record_close(ctxt);
                ret = ar_osal_thread_join_destroy(mcs_info->rec_ctxt->tid);
                ctxt->cur_state = MCS_STATE_IDLE;
                AR_LOG_INFO(LOG_TAG, "%s:%d Record stopped", __func__, __LINE__);
                return ret;
            }
            break;
        default:
            AR_LOG_ERR(LOG_TAG, "Invalid command");
            ret = AR_EFAILED;
            break;
    }
    AR_LOG_VERBOSE(LOG_TAG, "exit");
    return ret;
}

int32_t mcs_stream_cmd(uint32_t cmd, uint8_t *cmd_buf,
                     uint32_t cmd_buf_size, uint8_t*rsp_buf,
                     uint32_t rsp_buf_size, uint32_t *rsp_buf_bytes_filled)
{
    AR_LOG_INFO(LOG_TAG, "Enter %x cmd", cmd);
    int ret = 0;
    mcs_play_rec_t *pdata;
    ar_osal_mutex_lock(mcs_info->lock);

    switch(cmd) {
        case MCS_CMD_PLAY:
            AR_LOG_INFO(LOG_TAG, "MCS_CMD_PLAY");
            ret = process_playback_request(MCS_START, mcs_info->pb_ctxt,
                                    (mcs_play_t *) cmd_buf);
            break;
        case MCS_CMD_REC:
            AR_LOG_INFO(LOG_TAG, "MCS_CMD_REC");
            ret = process_record_request(MCS_START, mcs_info->rec_ctxt,
                                    (struct mcs_record_t*) cmd_buf);
            break;
        case MCS_CMD_PLAY_REC:
            pdata = (struct mcs_play_rec_t *)cmd_buf;
            ret = process_playback_request(MCS_START, mcs_info->pb_ctxt,
                                            &pdata->playback_session);
            if (ret == 0) {
                ret = process_record_request(MCS_START, mcs_info->rec_ctxt,
                                             &pdata->record_session);
                if (ret != 0) {
                    process_playback_request(MCS_STOP, mcs_info->pb_ctxt,
                                              &pdata->playback_session);
                }
            }
            break;
        case MCS_CMD_STOP:
            AR_LOG_INFO(LOG_TAG, "MCS_CMD_STOP");
            ret = process_playback_request(MCS_STOP, mcs_info->pb_ctxt,
                                           NULL);
            if (ret == 0)
                ret = process_record_request(MCS_STOP, mcs_info->rec_ctxt,
                                                NULL);
            break;

        default:
            AR_LOG_ERR(LOG_TAG, "%s: invalid command ID from ATS: 0x%x\n",
                                                            __func__, cmd);
            ret = AR_EFAILED;
            break;
    }
    ar_osal_mutex_unlock(mcs_info->lock);
    AR_LOG_INFO(LOG_TAG, "exit");
    return ret;
}

int32_t mcs_init(void)
{
    int ret = 0;
    int retry = 0;
    bool snd_card_found = false;
    char snd_macro[] = "snd";
    char *snd_card_name = NULL, *snd_card_name_t = NULL;
    char *snd_internal_name = NULL;
    char *tmp = NULL;
    char mixer_xml_file[MIXER_PATH_MAX_LENGTH] = {0};
    char mixer_xml_file_wo_variant[MIXER_PATH_MAX_LENGTH] = {0};
    char vendor_config_path[VENDOR_CONFIG_PATH_MAX_LENGTH] = {0};
    char file_name_extn[XML_PATH_EXTN_MAX_SIZE] = {0};
    char file_name_extn_wo_variant[XML_PATH_EXTN_MAX_SIZE] = {0};
    int snd_card;
    struct audio_mixer* tmp_mixer = NULL;

    do {
        /* Look for only default codec sound card */
        /* Ignore USB sound card if detected */
        snd_card = 0;
        while (snd_card < MAX_SND_CARD) {
            tmp_mixer = NULL;
            tmp_mixer = mixer_open(snd_card);
            if (tmp_mixer) {
                snd_card_name = strdup(mixer_get_name(tmp_mixer));
                if (!snd_card_name) {
                    AR_LOG_ERR(LOG_TAG, "failed to allocate memory for snd_card_name");
                    mixer_close(tmp_mixer);
                    ret = -EINVAL;
                    goto err_mcs;
                }
                AR_LOG_INFO(LOG_TAG, "mixer_open success. snd_card_num = %d, snd_card_name %s",
                snd_card, snd_card_name);

                /* TODO: Needs to extend for new targets */
                if (strstr(snd_card_name, "kona") ||
                    strstr(snd_card_name, "sm8150") ||
                    strstr(snd_card_name, "lahaina") ||
                    strstr(snd_card_name, "waipio") ||
                    strstr(snd_card_name, "kalama") ||
                    strstr(snd_card_name, "pineapple") ||
                    strstr(snd_card_name, "cliffs") ||
                    strstr(snd_card_name, "diwali") ||
                    strstr(snd_card_name, "anorak") ||
                    strstr(snd_card_name, "bengal") ||
                    strstr(snd_card_name, "monaco") ||
                    strstr(snd_card_name, "sun")) {
                    AR_LOG_VERBOSE(LOG_TAG, "Found Codec sound card");
                    snd_card_found = true;
                    audio_mixer = tmp_mixer;
                    break;
                } else {
                    if (snd_card_name) {
                        free(snd_card_name);
                        snd_card_name = NULL;
                    }
                    mixer_close(tmp_mixer);
                }
            }
            snd_card++;
        }

        if (!snd_card_found) {
            AR_LOG_INFO(LOG_TAG, "No audio mixer, retry %d", retry++);
            sleep(1);
        }
    } while (!snd_card_found && retry <= MAX_RETRY_CNT);


    if (snd_card >= MAX_SND_CARD || !audio_mixer) {
        AR_LOG_ERR(LOG_TAG, "audio mixer open failure");
        ret = -EINVAL;
        goto err_snd_name;
    }

    AR_LOG_INFO(LOG_TAG, "enter");
    mcs_info = calloc(1, sizeof(struct acdb_mcs));
    if (mcs_info == NULL) {
        AR_LOG_ERR(LOG_TAG, "%s: memory allocation failed.", __func__);
        ret = -AR_ENOMEMORY;
        goto err_snd_name;
    }

    mcs_info->pb_ctxt = calloc(1, sizeof(struct mcs_play_ctxt));
    if (mcs_info->pb_ctxt == NULL) {
        AR_LOG_ERR(LOG_TAG, "%s: memory allocation failed.", __func__);
        ret = -AR_ENOMEMORY;
        goto err_pb_ctxt;
    }
    mcs_info->pb_ctxt->cur_state = MCS_STATE_IDLE;

    mcs_info->rec_ctxt = calloc(1, sizeof(struct mcs_rec_ctxt));
    if (mcs_info->rec_ctxt == NULL) {
        AR_LOG_ERR(LOG_TAG, "%s: memory allocation failed.", __func__);
        ret = -AR_ENOMEMORY;
        goto err_rec_ctxt;
    }
    mcs_info->rec_ctxt->cur_state = MCS_STATE_IDLE;

    mcs_info->mixer = mixer_open(SND_CARD_VIRTUAL);
    if (!(mcs_info->mixer)) {
        AR_LOG_INFO(LOG_TAG, "Failed to open mixer\n");
        ret = -EINVAL;
        goto err_ret;
    }

    if (snd_card_name) {
        snd_card_name_t = strdup(snd_card_name);
    }
    if (!snd_card_name_t) {
        AR_LOG_ERR(LOG_TAG, "failed to allocate memory for snd_card_name_t");
        ret = -EINVAL;
        goto err_mixer_close;
    }
    snd_internal_name = strtok_r(snd_card_name_t, "-", &tmp);

    if (snd_internal_name != NULL)
        snd_internal_name = strtok_r(NULL, "-", &tmp);

    getFileNameExtn(snd_card_name, file_name_extn, file_name_extn_wo_variant);

    getVendorConfigPath(vendor_config_path, sizeof(vendor_config_path));

    /* Get path for mixer_xml_path_name in vendor */
    snprintf(mixer_xml_file, sizeof(mixer_xml_file),
             "%s/%s", vendor_config_path, MIXER_XML_BASE_STRING_NAME);

     strlcat(mixer_xml_file, MIXER_FILE_DELIMITER, MIXER_PATH_MAX_LENGTH);
     strlcat(mixer_xml_file_wo_variant, mixer_xml_file, XML_PATH_MAX_LENGTH);
     strlcat(mixer_xml_file, file_name_extn, MIXER_PATH_MAX_LENGTH);
     strlcat(mixer_xml_file_wo_variant, file_name_extn_wo_variant, XML_PATH_MAX_LENGTH);

     strlcat(mixer_xml_file, MIXER_FILE_EXT, MIXER_PATH_MAX_LENGTH);
     strlcat(mixer_xml_file_wo_variant, XML_FILE_EXT, XML_PATH_MAX_LENGTH);

    /* Get path for mcs defs xml name */
    snprintf(adev_info_xml_file, sizeof(adev_info_xml_file),
                    "%s/%s", vendor_config_path, ADEV_INFO_XMLFILE_BASE_STRING_NAME);
    strlcat(adev_info_xml_file, XML_FILE_DELIMITER, XML_PATH_MAX_LENGTH);
    strlcat(adev_info_xml_file, file_name_extn, XML_PATH_MAX_LENGTH);
    strlcat(adev_info_xml_file, XML_FILE_EXT, XML_PATH_MAX_LENGTH);
    AR_LOG_INFO(LOG_TAG, "mcs xml path : %s", adev_info_xml_file);
    list_init(&mcs_info->devs_list);

    if(access(adev_info_xml_file, F_OK ) == 0 ) {
        ret = parse_and_update_audio_defs(adev_info_xml_file);
        if(ret !=0) {
            AR_LOG_ERR(LOG_TAG, "mcs xml parsing failed with ret = %d", ret);
            ret = -EINVAL;
            goto err_audio_route;
        }
    }

    mcs_info->audio_route = audio_route_init(snd_card, mixer_xml_file);

    AR_LOG_INFO(LOG_TAG, "audio route %pK, mixer path %s",
                     mcs_info->audio_route, mixer_xml_file);
    if (!mcs_info->audio_route) {
        AR_LOG_ERR(LOG_TAG, "audio route init failed with %s trying with default variant %s",
                             mixer_xml_file, mixer_xml_file_wo_variant);
        mcs_info->audio_route = audio_route_init(snd_card, mixer_xml_file_wo_variant);
        AR_LOG_INFO(LOG_TAG, "audio route %pK, mixer path %s",
                     mcs_info->audio_route, mixer_xml_file_wo_variant);
        if (!mcs_info->audio_route) {
            AR_LOG_ERR(LOG_TAG, "audio route init failed with default variant %s",
                             mixer_xml_file_wo_variant);
            ret = -EINVAL;
            goto err_audio_route;
        }
    }
    ar_osal_mutex_create(&mcs_info->lock);

    ar_osal_thread_attr_init(&mcs_info->pb_ctxt->tattr);

    ar_osal_thread_attr_init(&mcs_info->rec_ctxt->tattr);

    mcs_info->pb_ctxt->tid = NULL;
    mcs_info->rec_ctxt->tid = NULL;
    if (snd_card_name_t) {
        free(snd_card_name_t);
        snd_card_name_t = NULL;
    }
    if (snd_card_name) {
        free(snd_card_name);
        snd_card_name = NULL;
    }
    AR_LOG_INFO(LOG_TAG, "exit");
    return 0;

err_audio_route:
    if (snd_card_name_t) {
        free(snd_card_name_t);
        snd_card_name_t = NULL;
    }
err_mixer_close:
    if(mcs_info->mixer) {
        mixer_close(mcs_info->mixer);
        mcs_info->mixer = NULL;
    }
err_ret:
    if(mcs_info->rec_ctxt) {
        free(mcs_info->rec_ctxt);
        mcs_info->rec_ctxt = NULL;
    }
err_rec_ctxt:
    if(mcs_info->pb_ctxt) {
        free(mcs_info->pb_ctxt);
        mcs_info->pb_ctxt = NULL;
    }
err_pb_ctxt:
    if(mcs_info) {
        free(mcs_info);
        mcs_info = NULL;
    }
err_snd_name:
    if (snd_card_name) {
        free(snd_card_name);
        snd_card_name = NULL;
    }
    if (audio_mixer) {
        mixer_close(audio_mixer);
        audio_mixer = NULL;
    }
err_mcs:
    return ret;
}

int32_t ats_mcs_deinit(void)
{
    int ret = 0;

    AR_LOG_INFO(LOG_TAG, "Enter");

    if(!mcs_info) {
        ret = AR_EBADPARAM;
        return ret;
    }
    audio_route_free(mcs_info->audio_route);

    ar_osal_mutex_destroy(mcs_info->lock);

    mixer_close(mcs_info->mixer);

    if (audio_mixer)
        mixer_close(audio_mixer);

    if (mcs_info->rec_ctxt) {
        free(mcs_info->rec_ctxt);
        mcs_info->rec_ctxt = NULL;
    }

    if (mcs_info->pb_ctxt) {
        free(mcs_info->pb_ctxt);
        mcs_info->pb_ctxt = NULL;
    }

    if(!list_empty(&mcs_info->devs_list)) {
        struct listnode *pv_pair_node, *temp;
        struct mcs_dev_def *pv_pair;
        list_for_each_safe(pv_pair_node, temp, &mcs_info->devs_list) {
            pv_pair = node_to_item(pv_pair_node, struct mcs_dev_def, list_node);
            list_remove(pv_pair_node);
            free(pv_pair);
        }
    }

    if (mcs_info) {
        free(mcs_info);
        mcs_info = NULL;
    }

    AR_LOG_INFO(LOG_TAG, "Exit");
    return ret;
}

/* Function to get audio vendor configs path */
void getVendorConfigPath (char* config_file_path, int path_size)
{
    char vendor_sku[PROPERTY_VALUE_MAX] = {'\0'};

    if (property_get("ro.boot.product.vendor.sku", vendor_sku, "") <= 0) {
#if defined(FEATURE_IPQ_OPENWRT) || defined(LINUX_ENABLED)
       /*Audio configs are stored in /etc */
       snprintf(config_file_path, path_size, "%s", "/etc");
#else
       /* Audio configs are stored in /vendor/etc */
       snprintf(config_file_path, path_size, "%s", "/vendor/etc");
#endif
    } else {
       /* Audio configs are stored in /vendor/etc/audio/sku_${vendor_sku} */
       snprintf(config_file_path, path_size,
                       "%s%s", "/vendor/etc/audio/sku_", vendor_sku);
    }
}

static int32_t convert_char_to_hex(char* num)
{
    uint64_t hexNum = 0;
    uint32_t base = 1;
    int32_t len = strlen(num);
    for (int i = len-1; i>=2; i--) {
        if (num[i] >= '0' && num[i] <= '9') {
            hexNum += (num[i] - 48) * base;
            base = base << 4;
        } else if (num[i] >= 'A' && num[i] <= 'F') {
            hexNum += (num[i] - 55) * base;
            base = base << 4;
        } else if (num[i] >= 'a' && num[i] <= 'f') {
            hexNum += (num[i] - 87) * base;
            base = base << 4;
        }
    }
    return (int32_t) hexNum;
}

static void process_device_data(struct platform_xml_data *data,const XML_Char **attr)
{
    struct mcs_dev_def *item = NULL;

    if (strcmp(attr[0], "key_value") != 0) {
        AR_LOG_ERR(LOG_TAG,"'key_value' not found");
        goto done;
    }

    if (strcmp(attr[2], "snd_device_name") != 0) {
        AR_LOG_ERR(LOG_TAG,"'snd_device_name' not found");
        goto done;
    }

    if (strcmp(attr[4], "back_end_name") != 0) {
        AR_LOG_ERR(LOG_TAG,"'back_end_name' not found");
        goto done;
    }

    item = calloc(1, sizeof(struct mcs_dev_def));
    if (!item) {
        AR_LOG_ERR(LOG_TAG, "Unable to allocate memory");
        return;
    }
    item->key_value = convert_char_to_hex(attr[1]);
    strlcpy(item->snd_device_name, attr[3], strlen(attr[3])+1);
    strlcpy(item->back_end_name, attr[5], strlen(attr[5])+1);
    list_add_tail(&mcs_info->devs_list, &item->list_node);
done:
    return;
}

static void start_tag(void *userdata, const XML_Char *tag_name,
    const XML_Char **attr)
{
    struct platform_xml_data *data = ( struct platform_xml_data *)userdata;

    AR_LOG_DEBUG(LOG_TAG,"start_tag: %s", tag_name);
    if (!strcmp(tag_name, "mcs_defs")) {
        return;
    } else if (!strcmp(tag_name, "device")) {
        process_device_data(data, attr);
    } else {
        AR_LOG_INFO(LOG_TAG,"No matching Tag found");
    }
}

static void end_tag(void *userdata, const XML_Char *tag_name)
{
    struct platform_xml_data *data = ( struct platform_xml_data *)userdata;

    AR_LOG_DEBUG(LOG_TAG,"end_tag: %s", tag_name);
    if (!strcmp(tag_name, "device")) {
        return;
    }
}

static void handle_data(void *userdata, const char *s, int len)
{
    struct platform_xml_data *data = (struct platform_xml_data *)userdata;

    if (len + data->offs >= sizeof(data->data_buf) ) {
       data->offs += len;
       /* string length overflow, return */
       return;
    } else {
        memcpy(data->data_buf + data->offs, s, len);
        data->offs += len;
    }
}

/* Function to parse xml file */
static int parse_and_update_audio_defs(char* adev_info_xml_file)
{
    XML_Parser parser;
    FILE *file = NULL;
    int ret = 0;
    int bytes_read;
    void *buf = NULL;
    struct platform_xml_data tag_data;

    memset(&tag_data, 0, sizeof(tag_data));
    AR_LOG_DEBUG(LOG_TAG,"XML parsing started %s", adev_info_xml_file);
    file = fopen(adev_info_xml_file, "r");

    if (!file) {
        AR_LOG_ERR(LOG_TAG,"Failed to open xml");
        ret = -EINVAL;
        goto done;
    }

    parser = XML_ParserCreate(NULL);
    if (!parser) {
        AR_LOG_ERR(LOG_TAG,"Failed to create XML");
        ret = -EINVAL;
        goto closeFile;
    }

    XML_SetUserData(parser,&tag_data);
    XML_SetElementHandler(parser, start_tag, end_tag);
    XML_SetCharacterDataHandler(parser, handle_data);

    while (1) {
        buf = XML_GetBuffer(parser, 1024);
        if (buf == NULL) {
            AR_LOG_ERR(LOG_TAG,"XML_Getbuffer failed");
            ret = -EINVAL;
            goto freeParser;
        }

        bytes_read = fread(buf, 1, 1024, file);
        if (bytes_read < 0) {
            AR_LOG_ERR(LOG_TAG,"fread failed");
            ret = -EINVAL;
            goto freeParser;
        }

        if (bytes_read == 0)
            break;

        if (XML_ParseBuffer(parser, bytes_read, bytes_read == 0) == XML_STATUS_ERROR) {
            AR_LOG_ERR(LOG_TAG,"XML ParseBuffer failed ");
            ret = -EINVAL;
            goto freeParser;
        }
    }

freeParser:
    if(ret != 0)
    {
        if(!list_empty(&mcs_info->devs_list)) {
            struct listnode *pv_pair_node, *temp;
            struct mcs_dev_def *pv_pair;
            list_for_each_safe(pv_pair_node, temp, &mcs_info->devs_list) {
                pv_pair = node_to_item(pv_pair_node, struct mcs_dev_def, list_node);
                list_remove(pv_pair_node);
                free(pv_pair);
            }
        }
    }

    XML_ParserFree(parser);
closeFile:
    fclose(file);
done:
    return ret;
}
